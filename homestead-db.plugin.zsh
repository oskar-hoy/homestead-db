# Export / Import databases on your Homestead box
# 
# Usage: db export
#        db import
function homestead-db() {(

    # Setup
    if [ -z "$HSDB_EXPORT_FOLDER" ]; then
        HSDB_EXPORT_FOLDER=~/Code/.db_export
    fi
    if [ -z "$HSDB_BLACKLIST" ]; then
        HSDB_BLACKLIST="mysql|information_schema|performance_schema|sys"
    fi

    # Get date / time
    TODAY_DATE=$(date +%Y-%m-%d)
    TODAY_TIME=$(date +%H.%m.%S)

    # Set filename for last dump data
    LAST_DUMP_FILE="$HSDB_EXPORT_FOLDER/last-dump.txt"

    case "$1" in
        # Display extended help
        "help")
            echo 'Homestead database helper'
            echo
            echo 'USAGE'
            echo "  $0 <command>"
            echo
            echo 'COMMANDS'
            echo '  export                         Export databases'
            echo '  import [ [database], ... ]     Import databases (Optionally select which to import)'
            echo '  drop <database> [table]        Drop a database or table'
            echo '  cli                            Log in to the MySQL console'
            echo '  help                           This message'
            echo
            exit
            ;;

        "export")
            # Calculate export path
            export_path="$HSDB_EXPORT_FOLDER/$TODAY_DATE/$TODAY_TIME"
            echo "Destination: $export_path"
            echo
            # Create export path
            if [ ! -d "$export_path" ]; then
                mkdir -p "$export_path"
            fi

            # Export each database
            for db in $(mysql --login-path=local -N -e 'show databases' | grep -viE "$HSDB_BLACKLIST") ; do
                echo "Exporting: $db"
                file_path="$export_path/$db.sql"
                mysqldump --login-path=local --databases $db > "$file_path"
            done

            # Write to last-dump.txt
            echo "$export_path" > $LAST_DUMP_FILE
            ;;

        "import")
            dump_path=$(head -1 $LAST_DUMP_FILE)
            echo "Reading from: $dump_path"
            echo

            # Get list of databases to import
            if [ $# -gt 1 ]; then
                shift
                for db_name in $@; do
                    filename=$(echo "$db_name.sql")
                    file_path="$dump_path/$filename"
                    if [ -f "$file_path" ]; then
                        echo "Importing: $db_name"
                        mysql --login-path=local < "$file_path"
                    fi
                done
            else
                for filename in $(ls -A $dump_path); do
                    db_name=$(echo "$filename" | sed 's/\.sql$//' | sed s/\n//g)
                    file_path="$dump_path/$filename"
                    if [ -f "$file_path" ]; then
                        echo "Importing: $db_name"
                        mysql --login-path=local < "$file_path"
                    fi
                done
            fi
            ;;

        "drop")
            shift
            database=$1
            shift
            table=$1
            
            if [ -z $database ]; then
                echo 'You must specify a database.'
                exit
            fi

            if [ ! -z $table ]; then
                echo "Removing table $table from database $database."
                mysql --login-path=local -e "USE $database; DROP TABLE $table"
                exit
            fi

            echo "Removing database $database"
            mysql --login-path=local -e "DROP DATABASE $database"
            ;;

        "cli")
            mysql --login-path=local
            ;;

        # Pass command to vagrant
        *)
            homestead-db help
            ;;
    esac
)}

alias db=homestead-db
