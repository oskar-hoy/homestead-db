# README #

Plugin for ZSH compatible frameworks, such as Oh my Zsh.

Easy export / import databases.

## Setup ##

* Clone repository to your ZSH custom plugins folder (Usually ~/.oh-my-zsh/custom/plugins)
* Add the plugin to your activated plugins list in .zshrc (homestead-db)
* Restart your terminal, or launch a new shell

## Configuration ##
You can override the destination path with the following environment variable:

```shell
HSDB_EXPORT_FOLDER=/home/vagrant/Code/.db-export
```

You can also edit the blacklist to skip more databases
```shell
HSDB_BLACKLIST="mysql|information_schema|performance_schema|sys"
```


## Usage ##

### Export databases ###

Will export all databases into individual files, in the output folder.
Each export is namespaced with a folder structure like this:

<path-to-export-folder>/<date>/<time>/

The path to the latest export is written to this file:

<path-to-export-folder>/last-dump.txt

```bash
$ db export
```

### Import databases ###

Imports all databases in the folder provided in the last-dump.txt file.

```bash
$ db import
```

To select which databases to import, just add them after the command.

```bash
$ db import homestead demos
```

### Drop table / database ###

To drop an entire database:

```bash
$ db drop homestead
```

To drop a table:

```bash
$ db drop homestead users
```

### Log in to MySQL console ###

Logs in to the MySQL console without credentials.

```
$ db

```

### Display help ###

Show usage.

```bash
$ db help
```
